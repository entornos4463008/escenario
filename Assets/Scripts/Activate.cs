using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Activate : MonoBehaviour
{ 
       public GameObject cinematica;
       public GameObject protagonista;
       public GameObject npc;

       private void OnTriggerEnter(Collider other)
       {
           if (other.gameObject.CompareTag("Player"))
           {
                npc.SetActive(false);
                protagonista.SetActive(false);
                cinematica.SetActive(true);
           }
       }
    
}
