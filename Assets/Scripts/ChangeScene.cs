using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public float tiempoEspera = 42f;

    void Start()
    {
        Invoke("CambiarDeEscena", tiempoEspera);
    }

    void CambiarDeEscena()
    {
        SceneManager.LoadScene(1);
    }
}
