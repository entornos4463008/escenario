using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorPies : MonoBehaviour
{
    public MovimientoPersonaje logicaPersonaje1;

    void Start()
    {
        
    }
 
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
        logicaPersonaje1.puedoSaltar = true;
    }

    private void OnTriggerExit(Collider other)
    {
        logicaPersonaje1.puedoSaltar = false;
    }
}
