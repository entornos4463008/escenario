using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float velocidadRotación = 200.0f;
    public float x, y;
    public float vM, hM;
    private Animator anim;

    public Rigidbody rb;
    public float fuerzaDeSalto = 8f;
    public bool puedoSaltar;

    public bool estoyAtacando;
    public bool avanzoSolo;
    public float impulsoDeGolpe = 10f;

    void Start()
    {
        puedoSaltar = false;
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (!estoyAtacando)
        {
            transform.Rotate(0, hM * Time.deltaTime * velocidadRotación, 0);
            transform.Translate(0, 0, y * Time.deltaTime * velocidadMovimiento);
            transform.Translate(x * Time.deltaTime * velocidadMovimiento, 0, 0);
        }
        
        if (avanzoSolo)
        {
            rb.velocity = transform.forward * impulsoDeGolpe;
        }

    }

    void Update()
    {
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        vM = Input.GetAxis("Mouse Y");
        hM = Input.GetAxis("Mouse X");

        if (Input.GetKeyDown(KeyCode.F) && puedoSaltar && !estoyAtacando)
        {
            anim.SetTrigger("Golpeo");
            estoyAtacando = true;
        }

        anim.SetFloat("velX", x);
        anim.SetFloat("velY", y);
        
        if (puedoSaltar)
        {
            if (!estoyAtacando)
            {
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    anim.SetBool("Salte", true);
                    rb.AddForce(new Vector3(0, fuerzaDeSalto, 0), ForceMode.Impulse);
                }

            }

            anim.SetBool("tocoSuelo", true);

        }
        else
        {
            EstoyCayendo();
        }
    }

    public void EstoyCayendo()
    {
        anim.SetBool("tocoSuelo", false);
        anim.SetBool("Salte", false);
    }

    public void DejaDeGolpear()
    {
        estoyAtacando = false;
        avanzoSolo = false;
    }

    public void AvanzoSolo()
    {
        avanzoSolo = true;
    }

    public void DejaDeAvanzar()
    {
        avanzoSolo = false;
    }

}
