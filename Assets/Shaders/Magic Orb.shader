Shader "Custom/OrbOfMagic" {
    Properties {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1, 1, 1, 1)
        _Radius ("Radius", Range(0, 1)) = 0.5
        _Strength ("Strength", Range(0, 10)) = 2.0
        _Speed ("Speed", Range(0, 10)) = 1.0
        _OutlineWidth ("Outline Width", Range(0, 0.1)) = 0.01
        _OutlineColor ("Outline Color", Color) = (0, 0, 0, 1)
    }

    SubShader {
        Tags { "RenderType"="Opaque" }

        CGPROGRAM
        #pragma surface surf Standard
        #pragma target 3.0

        sampler2D _MainTex;
        float _Radius;
        float _Strength;
        float _Speed;
        float _OutlineWidth;
        fixed4 _Color;
        fixed4 _OutlineColor;

        struct Input {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
            float3 worldRefl;
            float3 worldViewDir;
        };

        void surf (Input IN, inout SurfaceOutputStandard o) {
            // calculate distance from center
            float dist = length(IN.worldPos);

            // calculate the alpha based on distance and radius
            float alpha = smoothstep(_Radius - _Strength, _Radius, dist);

            // calculate wave animation offset based on position and time
            float waveOffset = sin(_Time.y * _Speed + IN.worldPos.x + IN.worldPos.z) * 0.1;
            float3 animatedPos = IN.worldPos + waveOffset;

            // calculate UV offset based on position and time
            float2 uvOffset = float2(sin(_Time.y * _Speed + IN.worldPos.x), cos(_Time.y * _Speed + IN.worldPos.z)) * 0.05;

            // set the surface color and texture
            o.Albedo = tex2D(_MainTex, IN.uv_MainTex + uvOffset).rgb * _Color;
            o.Alpha = alpha;

            // add emission to create a glow effect
            o.Emission = _Color.rgb * alpha * 0.5;
            o.Emission += sin(_Time.y * _Speed + IN.worldPos.x + IN.worldPos.y + IN.worldPos.z) * 0.1;

            // calculate outline
            float outline = 1.0 - smoothstep(_Radius - _Strength - _OutlineWidth, _Radius - _Strength, dist);
            o.Emission += _OutlineColor.rgb * outline;
        }
        ENDCG
    }

    FallBack "Diffuse"
}